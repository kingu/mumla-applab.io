
# Privacy policy

The Mumla Android mobile application ("Mumla") is built by Daniel Lublin and
released under a Free/Libre Open Source license. It is available at no cost,
donations are optional.

## Collection, use, and sharing of personal information

Mumla can record audio from a microphone on the device that it is running on.

A prerequisite for recording audio is that the user first has instructed Mumla
to connect to a third-party server of her own choice.

Depending on how Mumla is configured, recording of audio is carried out either
on the user's initiative (when tapping or pushing button) or by voice
activation (on high enough speech volume).

The recorded audio is immediately transmitted by Mumla to the third-party
server. The purpose of transmitting the audio this way, is to enable a dialogue
between the user of Mumla, and peers that are connected to the same third-party
server.

Mumla does not store the recorded audio on the device, or anywhere else.

## Changes to the policy

Any updates to the Privacy Policy will be made available on this page.

This policy is effective as of 2020-05-12. It has not been updated since.

## Contact

If you have any questions regarding this policy, do contact us at
mumla@lublin.se
