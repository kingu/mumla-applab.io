---
title: Mumla
lang: en
---

::: center
![](icon.png "Mumla icon")
:::

# Mumla

Mumla is an Android app for [Mumble](https://mumble.info/) -- the "free, open
source, low latency, high quality voice chat application". It is a fork of
[Plumble](https://github.com/acomminos/Plumble).

Source code repository: [https://gitlab.com/quite/mumla](https://gitlab.com/quite/mumla)

Issue tracker: [https://gitlab.com/quite/mumla/-/issues](https://gitlab.com/quite/mumla/-/issues)

::: center
[![homepage](https://fdroid.gitlab.io/artwork/badge/get-it-on.png){height=80}](https://f-droid.org/packages/se.lublin.mumla/ "Get it on F-Droid")
:::

Beta releases (installed side-by-side with the F-Droid release) are available
in [my own F-Droid repository](fdroidrepos://lublin.se/fdroid/repo?fingerprint=4FE75AB58C310E9778FBA716A0D1D66E8F49F697737BC0EE2437AEAE278CF64C).
That's an fdroidrepos-link, to be followed on a device with F-Droid installed.
As QR code:

::: center
![](lublin-fdroid-repo-qrcode.png "F-Droid repository URL as QR code")
:::

___

[site repo](https://gitlab.com/mumla-app/mumla-app.gitlab.io)
